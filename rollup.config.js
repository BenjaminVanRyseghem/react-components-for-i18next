/* eslint-disable filenames/match-regex */
import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import replace from "rollup-plugin-replace";
import resolve from "rollup-plugin-node-resolve";

const NODE_ENV = process.env.NODE_ENV || "development";
const outputFile = NODE_ENV === "production" ? "./dist/prod.js" : "./dist/dev.js";

export default [
	{
		input: "src/index.js",
		output: {
			name: "index",
			file: outputFile,
			format: "es",
			globals: {
				react: "React",
				"prop-types": "PropTypes"
			},
			exports: "named"
		},
		plugins: [
			replace({
				"process.env.NODE_ENV": JSON.stringify(NODE_ENV)
			}),
			babel({
				exclude: "node_modules/**"
			}),
			resolve(),
			commonjs()
		],
		external: [
			"react",
			"prop-types"
		]
	}
];
