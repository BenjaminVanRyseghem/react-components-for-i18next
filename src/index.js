import * as ReactI18Next from "react-i18next";
import Interpolate from "./interpolate";
import Translate from "./translate";

export { Interpolate, ReactI18Next };
export default Translate;
