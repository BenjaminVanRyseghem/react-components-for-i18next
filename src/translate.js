import { I18n } from "react-i18next";
import PropTypes from "prop-types";
import React from "react";

/**
 * Take care of translation through i18next.
 * @extends {React.Component}
 */
class InnerTranslate extends React.Component {
	/**
	 * Render the translated key
	 * @return {PropTypes.node} Translated key
	 */
	render() {
		return this.props.t(this.props.i18nKey, Object.assign({}, this.props, { defaultValue: this.props.children }));
	}
}

InnerTranslate.propTypes = {
	children: PropTypes.string.isRequired,
	i18nKey: PropTypes.string.isRequired,
	i18next: PropTypes.object.isRequired,
	t: PropTypes.func.isRequired // eslint-disable-line id-length
};

/**
 * Translation react component providing a default text (in french).
 *
 * @extends {React.Component}
 * @example
 *
 * <Translate i18nKey="hello" name="Ben">
 *     Salut %name%
 * </Translate>
 */
export default class Translate extends React.Component {
	/**
	 * Render an {@link InnerTranslate} node with i18n props
	 * @return {React.node} Children wrapped with i18h props
	 */
	render() {
		return (
			<I18n>
				{(t, i18next) => (
					<InnerTranslate {...this.props} i18next={i18next} t={t}>
						{this.props.children}
					</InnerTranslate>
				)}
			</I18n>
		);
	}
}

Translate.propTypes = {
	children: PropTypes.string.isRequired,
	i18nKey: PropTypes.string.isRequired
};
